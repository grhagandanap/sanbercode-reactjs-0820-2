import React from 'react';
import './tugas9.css'

class Sembilan extends React.Component {
  render () {
    return (
    <>
      <div className="rangka">
        <h1 style={{textAlign: "center"}}>Form Pembelian Buah</h1>
        <form style={{paddingLeft: 30}}>
            <table style={{borderStyle: 0}}>
                <tr>
                    <td style={{width: 150}}><label><b>Nama Pelanggan</b></label></td>
                    <td><input type="text" /></td>
                </tr>
                <tr>
                    <td style={{verticalAlign: "bottom"}}><label><b>Daftar Item</b></label></td>
                    <td>
                        <br/>
                        <input type="checkbox"/>
                        <label>Semangka</label><br/>
                        <input type="checkbox"/>
                        <label>Jeruk</label><br/>
                        <input type="checkbox"/>
                        <label>Nanas</label><br/>
                        <input type="checkbox"/>
                        <label>Salak</label><br/>
                        <input type="checkbox"/>
                        <label>Anggur</label><br/>
                    </td>
                </tr>
            </table>
            <br/>
            <input type="submit" value="Kirim" style={{borderRadius: 30}}/><br/><br/>  
        </form>
      </div> 
    </>
    );
  }
}

export default Sembilan;