import React, {useState, useEffect} from "react"
import axios from "axios"

const DaftarBuah = () => {

  const [daftarBuah, setDaftarBuah] = useState(null)
  const [inputNama, setInputNama] = useState("")
  const [inputHarga, setInputHarga] = useState("")
  const [inputBerat, setInputBerat] = useState(0)
 
  useEffect (() => {
    if (daftarBuah === null) {
        axios.get (`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
            setDaftarBuah(res.data)
        })
    }}, [daftarBuah])

  const handleDelete = (event) => {
    var ID_FRUIT = parseInt(event.target.value)
    axios.delete (`http://backendexample.sanbercloud.com/api/fruits/${ID_FRUIT}`)
    .then(res => {
        var newDaftarBuah =daftarBuah.filter(x=>x.id !== ID_FRUIT)
        setDaftarBuah (newDaftarBuah)
    })
  }   
    
  const handleEdit = (event) => {
    var idBuah = parseInt(event.target.value)
    var buah = daftarBuah.find(x=>x.id === idBuah)
    setInputNama(buah.name)
    setInputHarga(buah.price)
    setInputBerat (buah.weight)
  }

  const handleChange = (event) => {
    let typeOfInput = event.target.name
    switch (typeOfInput){
      case "name":
      {
         setInputNama({buah.name: typeOfInput})
        break
      }
      case "price":
      {
        setInputHarga({buah.price: typeOfInput})
        break
      }
      case "weight":
      {
        setInputBerat({buah.weight: typeOfInput})
          break
      }
    default:
      {break;}
    }
  }

  const handleSubmit = (event) => {
    axios.post (`http://backendexample.sanbercloud.com/api/fruits`)
    .then(res=>{

    })
    event.preventDefault()

    let nama = inputName
    let harga = this.state.inputHarga.toString()
    let berat = this.state.inputBerat

    console.log(this.state)

    if (nama.replace(/\s/g,'') !== "" && harga.replace(/\s/g,'') !== ""){      
      let newDaftarBuah = this.state.daftarBuah
      let index = this.state.indexOfForm
      
      if (index === -1){
        newDaftarBuah = [...newDaftarBuah, {nama, harga, berat}]
      }else{
        newDaftarBuah[index] = {nama, harga, berat}
      }
  
      this.setState({
        daftarBuah: newDaftarBuah,
        inputName: "",
        inputHarga: "",
        inputBerat: 0
      })
    }
  }

    return(
      <>
        <h1 style={{textAlign: "center"}}>Daftar Harga Buah</h1>
        <div style={{width: "50%", margin: "0 auto"}}>
        <table style={{padding: 2,border: "2px solid black"}}>
          <thead>
            <tr style={{backgroundColor: "rgb(171, 167, 166)"}}>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
              {
               daftarBuah !== null && daftarBuah.map((item, index)=>{
                  return(                    
                    <tr style={{backgroundColor: "rgb(227, 122, 64)"}} key={item.id}>
                      <td>{index+1}</td>
                      <td>{item.name}</td>
                      <td>{item.price}</td>
                      <td>{item.weight/1000} kg</td>
                      <td>
                        <button onClick={handleEdit} value={item.id}>Edit</button>
                        &nbsp;
                        <button onClick={handleDelete} value={item.id}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
          </tbody>
        </table>
        </div>
        {/* Form */}
        <h1 style={{textAlign: "center"}}>Form Daftar Harga Buah</h1>
        <div style={{width: "50%", margin: "0 auto", display: "block"}}>
          <div style={{border: "1px solid #aaa", padding: "20px"}}>
            <form onSubmit={handleSubmit}>
              <label style={{float: "left"}}>
                Nama:
              </label>
              <input style={{float: "right"}} type="text" name="name" value={inputNama} onChange={handleChange}/>
              <br/>
              <br/>
              <label style={{float: "left"}}>
                Harga:
              </label>
              <input style={{float: "right"}} type="text" name="harga" value={inputHarga} onChange={handleChange}/>
              <br/>
              <br/>
              <label style={{float: "left"}}>
                Berat (dalam gram):
              </label>
              <input style={{float: "right"}} type="number" name="berat" value={inputBerat} onChange={handleChange}/>
              <br/>
              <br/>
              <div style={{width: "100%", paddingBottom: "20px"}}>
                <button style={{ float: "right"}}>submit</button>
              </div>
            </form>
          </div>
        </div>
      </>
    )  
}

export default DaftarBuah
