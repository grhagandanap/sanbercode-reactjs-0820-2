import React from 'react';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

class Buah extends React.Component {
    render () {
        return (
            <>
            <div style={{fontFamily:"Times New Roman"}}>
                <h1 style={{textAlign: "center"}}>Tabel Harga Buah</h1>
        
                <table style={{marginLeft: 300, border: "2px solid black"}}>
                    <tr style={{backgroundColor: "rgb(171, 167, 166)"}}>
                        <th style={{width: 275}}><b>Nama</b></th>
                        <th style={{width: 175}}><b>Harga</b></th>
                        <th style={{width: 175}}><b>Berat</b></th>
                    </tr>
                    
                    {dataHargaBuah.map((buah, index) => (
                     <tr style={{backgroundColor: "rgb(227, 122, 64)"}} key={index}>
                         <td>{buah.nama}</td>
                         <td>{buah.harga}</td>
                         <td>{buah.berat/1000} kg</td>
                     </tr>
                    ))}
                </table>
            </div>
            </>
        )
    }
}

export default Buah;
